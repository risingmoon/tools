PRE_PROMPT="\[\033[01;34m\]--J-LEE--\[\033[0m\]"

export COUCHBASE_HOST=couchbase1
alias cbq="/opt/couchbase/bin/cbq -engine=http://$COUCHBASE_HOST:8093"

#jump alias
alias pop='jump  ~/pokemon.com/main/pokemon_ui/pop-build/local/pokemon_ui/static2/_ui/js/'
alias op='jump ~/pokemon.com/op'
alias main='jump ~/pokemon.com/main'
alias ptcs='jump ~/pokemon.com/ptcs'
alias working='jump ~/pokemon.com/main/pokemon_ui/working'
alias pcom='jump ~/pokemon.com'
alias pui='jump ~/pokemon.com/main/pokemon_ui'
alias tools="jump ~/tools"
alias tomcat='jump /var/lib/tomcat6/webapps/sso/WEB-INF/view/jsp'
alias ans='jump /home/tpci/tpci-ansible'


#django shortcuts
alias opman='sudo python -Wi ~/pokemon.com/op/manage.py'
alias mainman='sudo python -Wi ~/pokemon.com/main/manage.py'
alias runserver="sudo python -Wi manage.py runserver 0.0.0.0:8000"
alias manage="sudo python -Wi manage.py"
alias shell="sudo python -Wi manage.py shell"
alias fn="find . -name"

#git flow
alias ff="git flow feature"
alias fr="git flow release"
alias fh="git flow release"

# ssh
alias rundeck="ssh tpci@rundeck.pokemon.biz"
alias sshstaging="ssh staging-bastion.pokemon.biz"
alias sshdjango="ssh django@j-lee-aws.pokemon.biz"
alias sshtpci="ssh tpci@j-lee-aws.pokemon.biz"

function task() {
   pushd ~/pokemon.com/main/pokemon_ui/tasks
   grunt $1
   popd
}

#Alfresco
function avm() {

   if [ -z "$2" ]; then
       cd /opt/pokemon/alfresco/pokemon-"$1"/
   else
       cd /opt/pokemon/alfresco/pokemon-"$1"/$2
   fi

}

#DOCKER
function tpci() {
   if [ -z "$1" ]; then
     sudo -iu tpci
   else
     sudo -iu tpci ssh -t $1 'cd ../django/pokemon.com/; bash'
   fi
}
alias rebuildcas="tpci_lxd -t rebuild-cas provision sso1 -p sso"
alias sunmoon="ssh -i ~/.ssh/niji-cms-production ec2-user@52.43.211.25"

# Gradle
if [ -d "/opt/gradle" ]; then
    export GRADLE_HOME="/opt/gradle"
    PATH="$PATH:$GRADLE_HOME/gradle-4.2.1/bin"
    export GRADLE_USER_HOME="$HOME/.gradle"
fi
