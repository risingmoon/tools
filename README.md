# Setup

* Add tools/.bashrc
* Install Synastic

```
mkdir -p ~/.vim/autoload ~/.vim/bundle && \
curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim
```

* copy .vimrc
* install synastic

```
cd ~/.vim/bundle && \
git clone --depth=1 https://github.com/vim-syntastic/syntastic.git
```

* install flake8, pylint
```
pip install flake8
```

* install virtualenvwrapper
```
pip install virtualenvwrapper
```
