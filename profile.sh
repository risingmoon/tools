export GIT_EDITOR=vim
TOOLS=$HOME/tools
# Git Bash Autocomplete
# https://raw.githubusercontent.com/git/git/master/contrib/completion/git-completion.bash
source $TOOLS/.git-completion.sh


# Git Prompt
# https://raw.github.com/git/git/master/contrib/completion/git-prompt.sh
source $TOOLS/.git-prompt.sh

# Git Flow Scripts
# source $TOOLS/flow.sh

# PS1='[\u@\h \W$(__git_ps1 " (%s)")]\$ '
GIT_PS1_SHOWDIRTYSTATE=1
GIT_PS1_SHOWCOLORHINTS=1
GIT_PS1_SHOWSTASHSTATE=1
GIT_PS1_SHOWUPSTREAM="auto"
Color_Off="\[\033[0m\]"
Yellow="\[\033[0;33m\]"
PROMPT_BASE="$PRE_PROMPT\[\033[01;32m\]\u@\h\[\033[0m\]:\[\033[01;34m\]\w\[\033[0m\]$"
PROMPT_COMMAND='__git_ps1 "${VIRTUAL_ENV:+[$Yellow`basename $VIRTUAL_ENV`$Color_Off]\n}" $PROMPT_BASE "[%s]\n"'

# python settings
export PYTHONDONTWRITEBYTECODE=1

# virtualenv settings
export WORKON_HOME=$HOME/.virtualenvs
export PROJECT_HOME=$HOME
source /usr/local/bin/virtualenvwrapper.sh

# miscellaneous
alias ..="cd .."
alias .2="cd ../.."
alias .3="cd ../../.."
alias tree="tree --charset=ASCII"

# grep
alias histg="history | grep"
alias pyg="grepper py"
alias hyg="grepper html"
alias jg="grepper js"

# vimtools
alias vimbash="vimtools bash.sh"
alias vimpokemon="vimtools pokemon.sh"

# GREPPER

function grepper() {
    grep -rn $2 $3 --include=*.$1
}

function vimtools() {
    sudo vim $TOOLS/$1
    . ~/.bashrc
}
# JUMP
function jump() {
   if [ -z "$2" ]; then
       cd $1
   else
       cd $1/$2
   fi
}
