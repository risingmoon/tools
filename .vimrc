execute pathogen#infect()
syntax on
filetype plugin indent on
au FileType python setl st=4 sw=4 et
setl st=4 sw=4 et
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 0
let g:syntastic_check_on_wq = 1
let g:syntastic_python_checkers = ['flake8']
"let g:syntastic_java_javac_classpath = '/home/tpci/cas-server/build/classes/main/:/home/tpci/cas-server/WebContent/WEB-INF/lib/*.jar'
